/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package complex;

import common.DependencyException;
import implementations.ImplementationA1;
import implementations.ImplementationB1;
import implementations.ImplementationC1;
import implementations.ImplementationD1;
import interfaces.InterfaceA;
import interfaces.InterfaceB;
import interfaces.InterfaceC;
import interfaces.InterfaceD;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author santi
 */
public class ContainerTest {
    
    private Injector injector;
    private InterfaceA a;
    private InterfaceB b;
    private InterfaceC c;
    private InterfaceD d;
    /**
     *
     */
    public ContainerTest() {
    }

    /**
     * Initialize the Injector, we use the singleton pattern
     */
    @Before
    public void init(){
        this.injector = Container.getInstance();
    }

    /**
     * Check if the constant that register is the same that obtain.
     * @throws DependencyException
     */
    @Test
    public void testRegisterConstant() throws DependencyException{
        injector.registerConstant(Integer.class, 42);
        assertThat("Isn't the same", injector.getObject(Integer.class), is(42));
    }

    /**
     * When we try this, you must be receive an exception because the constant exists.
     * @throws DependencyException
     */
    @Test (expected = DependencyException.class)
    public void testRegisterConstantThatExists() throws DependencyException{
        injector.registerConstant(Integer.class, 42);
        injector.registerConstant(Integer.class, 42);
    }
    
    /**
     * Check register a Factory type A1
     * @throws common.DependencyException
     */
    @Test
    public void testRegisterFactoryFactoryA1()throws DependencyException{
        injector.registerConstant(InterfaceB.class, b);
        injector.registerConstant(InterfaceC.class, c);
        injector.registerFactory(InterfaceA.class, new FactoryA1(), InterfaceB.class, InterfaceC.class);
        InterfaceA a = (InterfaceA) injector.getObject(InterfaceA.class);
        assertThat(a, is(instanceOf(ImplementationA1.class)));
        ImplementationA1 a1 =(ImplementationA1)a;
        assertThat(a1.b, is(b));
        assertThat(a1.c, is(c));
    }

    /**
     * Check register a Factory type B1
     * @throws DependencyException
     */
    @Test
    public void testRegisterFactoryFactoryB1()throws DependencyException{
        injector.registerConstant(InterfaceD.class, d);
        injector.registerFactory(InterfaceB.class, new FactoryB1(), InterfaceD.class);
        b = (InterfaceB)injector.getObject(InterfaceB.class);
        assertThat(b, is(instanceOf(ImplementationB1.class)));
        ImplementationB1 b1 = (ImplementationB1)b;
        assertThat(b1.d, is(d));
    }
    
    /**
     * Check register a Factory type C1
     * @throws DependencyException
     */
    @Test
    public void testRegisterFactoryFactoryC1()throws  DependencyException{
        
        injector.registerConstant(String.class, "Hay que seguir duro");
        injector.registerFactory(InterfaceC.class, new FactoryC1(), String.class);
        
        c = (InterfaceC) this.injector.getObject(InterfaceC.class);
        assertThat(c, is(instanceOf(ImplementationC1.class)));
        
        ImplementationC1 c1 = (ImplementationC1) this.c;
        assertThat(c1.s, is("Hay que seguir duro"));
    }
    /**
     * Check register a Factory type D1
     * @throws Exception
     */
    @Test
    public void testRegisterFactoryFactoryD1() throws Exception{
           
        injector.registerConstant(Integer.class, 42);
        injector.registerFactory(InterfaceD.class, new FactoryD1(), Integer.class);
        InterfaceD d = (InterfaceD) injector.getObject(InterfaceD.class);
        assertThat(d, is(instanceOf(ImplementationD1.class)));
        ImplementationD1 d1 = (ImplementationD1) d;
        assertThat(d1.i, is(42));
    }
    
    /**
     * Check if you can register a Factory that exists
     * @throws DependencyException
     */
    @Test(expected = DependencyException.class)
    public void testRegisterFactoryThatExist() throws DependencyException{
        
        injector.registerConstant(Integer.class, 42);
        injector.registerFactory(InterfaceD.class, new FactoryD1(), 
                Integer.class);
        injector.registerFactory(InterfaceD.class, new FactoryD1(), 
                Integer.class);
    }
    
    /**
     * Check register a factory without a registered constant
     * @throws DependencyException
     */
    @Test(expected = DependencyException.class)
    public void testRegisterFactoryWithParametersNotRegistered()
            throws DependencyException{
        injector.registerFactory(InterfaceD.class, new FactoryD1(),
                Integer.class);
    }
    
    /**
     * Check register a Factory without parameters
     * @throws DependencyException
     */
    @Test(expected = DependencyException.class)
    public void testRegisterFactoryWithoutParameters()throws DependencyException{
        injector.registerConstant(Integer.class, 42);
        injector.registerFactory(InterfaceD.class, new FactoryD1());
    }
    
    /**
     * Check if method's getObject works correctly
     * @throws DependencyException
    */
    @Test
    public void testGetObject() throws DependencyException{
        injector.registerConstant(Integer.class, 911);
        injector.registerFactory(InterfaceD.class, new FactoryD1(), 
                Integer.class);
        Object o = injector.getObject(Integer.class);
        InterfaceD f = (InterfaceD) injector.getObject(InterfaceD.class);
        assertEquals("Isn't the same object", 911, o);
        assertThat("Not obtain an instance about ImplementationD",f, is(instanceOf(ImplementationD1.class)));
    }
    
    /**
     * Check if not exist the name associated
     * @throws DependencyException
    */
    @Test(expected = DependencyException.class)
    public void testGetObjectThatNotExist() throws DependencyException{
        injector.getObject(InterfaceD.class);
    }
}
