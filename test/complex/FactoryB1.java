/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package complex;

import common.DependencyException;
import implementations.ImplementationB1;
import interfaces.InterfaceD;

/**
 *
 * @author santi
 */
public class FactoryB1 implements Factory<ImplementationB1>{

    @Override
    public ImplementationB1 create(Object... parameters) throws DependencyException {
        InterfaceD d;
        try{
            d = (InterfaceD)parameters[0];
        }catch(ClassCastException | IndexOutOfBoundsException ex){
            throw new DependencyException(ex);
        }
        return new ImplementationB1(d);
    }
    
}
