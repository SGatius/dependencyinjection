/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package complex;

import common.DependencyException;
import implementations.ImplementationA1;
import interfaces.InterfaceB;
import interfaces.InterfaceC;
/**
 *
 * @author santi
 */
public class FactoryA1 implements Factory<ImplementationA1>{

    @Override
    public ImplementationA1 create(Object... parameters) throws DependencyException {
                InterfaceB b;
        InterfaceC c;
        try{
            b = (InterfaceB)parameters[0];
            c = (InterfaceC)parameters[1];
        }catch(ClassCastException | IndexOutOfBoundsException ex){
            throw new DependencyException(ex);
        }
        return new ImplementationA1(b, c);
    }
    
}
