/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package complex;

import common.DependencyException;
import implementations.ImplementationC1;

/**
 *
 * @author santi
 */
public class FactoryC1 implements Factory<ImplementationC1>{

    @Override
    public ImplementationC1 create(Object... parameters) throws DependencyException {
        String s;
        try{
            s = (String)parameters[0];
        }catch(ClassCastException | IndexOutOfBoundsException ex){
            throw new DependencyException(ex);
        }
        return new ImplementationC1(s);
    }
    
}
