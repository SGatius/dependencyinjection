/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import common.DependencyException;
import implementations.FactoryA1;
import implementations.FactoryB1;
import implementations.FactoryC1;
import implementations.FactoryD1;
import implementations.ImplementationA1;
import implementations.ImplementationB1;
import implementations.ImplementationC1;
import implementations.ImplementationD1;
import interfaces.InterfaceA;
import interfaces.InterfaceB;
import interfaces.InterfaceC;
import interfaces.InterfaceD;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

/**
 *
 * @author santi
 */
public class ContainerTest {
    private Injector injector;
    private InterfaceB b;
    private InterfaceC c;
    private InterfaceD d;
    
    /**
     *
     */
    public ContainerTest() {
    }

    /**
     *
     */
    @Before
    public void init(){
        this.injector = Container.getInstance();
    }
    
    /**
     * Check if register a constant, this constant is saved
     * @throws DependencyException
     */
    @Test
    public void testRegisterConstant() throws DependencyException{
        injector.registerConstant("I", 42);
        assertThat("Isn't the same", injector.getObject("I"), is(42));
    }
    
    /**
     * Check if you can register a constant that exists
     * @throws DependencyException
     */
    @Test(expected = DependencyException.class)
    public void testRegisterConstantThatExist() throws DependencyException{
        injector.registerConstant("I", 42);
    }
    
    /**
     * Check register a Factory type A1
     * @throws Exception
     */
    @Test
    public void testRegisterFactoryFactoryA1()throws Exception{
        injector.registerConstant("B", b);
        injector.registerConstant("C", c);
        injector.registerFactory("A", new FactoryA1(), "B", "C");
        InterfaceA a = (InterfaceA) injector.getObject("A");
        assertThat(a, is(instanceOf(ImplementationA1.class)));
        ImplementationA1 a1 =(ImplementationA1)a;
        assertThat(a1.b, is(b));
        assertThat(a1.c, is(c));
    }
    
    /**
     * Check register a Factory type B1
     * @throws Exception
     */
    @Test
    public void testRegisterFactoryFactoryB1()throws Exception{
        
        injector.registerConstant("D", this.d);
        
        injector.registerFactory("B1", new FactoryB1(), "D");
        
        b = (InterfaceB) this.injector.getObject("B1");
        assertThat(this.b, is(instanceOf(ImplementationB1.class)));
        
        ImplementationB1 b1 = (ImplementationB1) this.b;
        assertThat(b1.d, is(this.d));
    }
    
    /**
     * Check register a Factory type C1
     * @throws Exception
     */
    @Test
    public void testRegisterFactoryFactoryC1()throws Exception{
        
        injector.registerConstant("NAME", "Santi Gatius");
        
        injector.registerFactory("C1", new FactoryC1(), "NAME");
        
        c = (InterfaceC) this.injector.getObject("C1");
        assertThat(this.c, is(instanceOf(ImplementationC1.class)));
        
        ImplementationC1 c1 = (ImplementationC1)this.c;
        assertThat(c1.s, is("Santi Gatius"));
    }
    
    /**
     * Check register a Factory type D1
     * @throws Exception
     */
    @Test
    public void testRegisterFactoryFactoryD1() throws Exception{
   
        injector.registerFactory("D1", new FactoryD1(), "I");
        InterfaceD d = (InterfaceD) injector.getObject("D1");
        assertThat(d, is(instanceOf(ImplementationD1.class)));
        ImplementationD1 d1 = (ImplementationD1) d;
        assertThat(d1.i, is(42));
    }
    
    /**
     * Check if you can register a Factory that exists
     * @throws DependencyException
     */
    @Test(expected = DependencyException.class)
    public void testRegisterFactoryThatExist() throws DependencyException{
        injector.registerFactory("D1", new FactoryD1(), "");
    }
    
    /**
     * Check if you can register a Factory without parameters
     * @throws DependencyException
     */
    @Test(expected = DependencyException.class)
    public void testRegisterFactoryWithEmptyParameters() throws DependencyException{
        injector.registerFactory("V", new FactoryA1(), "");
    }
    
    /**
     * Check if you can register a Factory with bad parameters
     * @throws DependencyException
     */
    @Test(expected = DependencyException.class)
    public void testRegisterFactoryWithBadParameters() throws DependencyException{
        injector.registerFactory("V", new FactoryA1(), "V");
    }
    
    /**
     * Check if method's getObject works correctly
     * @throws DependencyException
     */
    @Test
    public void testGetObject() throws DependencyException{
        injector.registerConstant("CONSTANT", 911);
        injector.registerFactory("FACTORY", new FactoryD1(), "CONSTANT");
        Object c = injector.getObject("CONSTANT");
        InterfaceD f = (InterfaceD) injector.getObject("FACTORY");
        assertEquals("Isn't the same object", 911, c);
        assertThat("Not obtain an instance about ImplementationD",
                f, is(instanceOf(ImplementationD1.class)));
    }

    /**
     * Check if not exist the name associated
     * @throws DependencyException
     */
    @Test(expected = DependencyException.class)
    public void testGetObjectThatNotExist() throws DependencyException{
        injector.getObject("LOW");
    }
    
}
