/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import common.DependencyException;

/**
 *
 * @author santi
 */
public interface Containers {
    
    /**
     *
     * @param name
     * @return a boolean that check name if is already exists.
     */
    boolean checkName(String name);

    /**
     *
     * @param name
     * @param value
     * @return null if not exist a value associated with name. If exists return the corresponding object
     */
    Object addChecking(String name, Object value);

    /**
     *
     * @param name
     * @return the corresponding object
     */
    Object get(String name);
    
    /**
     * Check if exists the parameters on ContainerConstants and return it
     * @param params
     * @return the parameters about ContainerFactory
     * @throws DependencyException
     */
    Object[] checkToGetParameters(Object[] params)throws DependencyException;
    
    <E> boolean checkName(Class<E> name) throws DependencyException;
    <E> E addChecking(Class<E> name, E value);
    <E> E get(Class<E> name);

    <E> E addChecking(Class<E> name, FactoryParamsE fp);

    public Object[] checkToGetParametersE(Object[] parameters);
}
