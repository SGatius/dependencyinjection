/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import common.Containers;
import common.DependencyException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author santi
 */
public class ContainersConstants implements Containers{

    private Map constants;
    
    public ContainersConstants() {
       constants = new HashMap();
    }
    
    @Override
    public boolean checkName(String name) {
        return constants.containsKey(name);  
    }

    @Override
    public Object addChecking(String name, Object value) {
        return constants.putIfAbsent(name, value);
    }
        
    @Override
    public Object get(String name) {
        return constants.get(name);
    }
    
    @Override
    public Object[] checkToGetParameters(Object[] params) 
            throws DependencyException{
        Object[] parameters = new Object[params.length];
        int pos = 0;
        for(Object p : params){
            if(constants.containsKey(p)){
                parameters[pos] = constants.get((String)p);
                pos = pos + 1;
            }else{
                return null;
            }
        }
        return parameters;
    }
    
    @Override
    public Object[] checkToGetParametersE(Object[] params){
        Object[] parameters = new Object[params.length];
        int pos = 0;
        for(Object p : params){
            if(constants.containsKey(p)){
                parameters[pos] = constants.get(p);
                pos = pos + 1;
            }else{
                return null;
            }
        }
        return parameters;
    }

    @Override
    public <E> boolean checkName(Class<E> name) throws DependencyException {
        return constants.containsKey(name);
    }

    @Override
    public <E> E addChecking(Class<E> name, E value) {
        return (E) constants.putIfAbsent(name, value);
    }

    @Override
    public <E> E get(Class<E> name) {
        return (E) constants.get(name);
    }

    @Override
    public <E> E addChecking(Class<E> name, FactoryParamsE fp) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
