/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import complex.Factory;

/**
 *
 * @author santi
 */
public class FactoryParamsE {
      
    private Factory creator;
    private Object[] parameters;

    /**
     * Constructor
     * @param creator
     * @param parameters
     */
    public FactoryParamsE(Factory creator, Object[] parameters) {
        this.creator = creator;
        this.parameters = parameters;
    }
        
    /**
     * Method to get a Factory's object
     * @return a Factory
     */
    public Factory getCreator() {
        return creator;
    }

    /**
     * Method to get the parameters
     * @return parameters
     */
    public Object[] getParameters() {
        return parameters;
    }  
}
