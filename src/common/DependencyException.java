/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

/**
 *
 * @author santi
 */
public class DependencyException extends Exception{
    
    /**
     *
     * @param cause
     */
    public DependencyException(Exception cause){
        super(cause);
    }
    
    /**
     *
     * @param message
     */
    public DependencyException(String message){
        super(message);
    }
    
}
