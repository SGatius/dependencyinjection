/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package common;

import common.Containers;
import common.DependencyException;
import common.FactoryParams;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author santi
 */
public class ContainersFactories implements Containers{
        
    private Map factories;
    
    public ContainersFactories() {
       factories = new HashMap();
    }
    
    @Override
    public boolean checkName(String name) {
        return factories.containsKey(name);  
    }
 
    @Override
    public Object addChecking(String name, Object value) {
        return factories.putIfAbsent(name, value);
    }

    @Override
    public Object get(String name) {
        return (FactoryParams)factories.get(name);
    }

    @Override
    public Object[] checkToGetParameters(Object[] params) throws DependencyException {
        throw new UnsupportedOperationException("Not supported here.");
    }

    @Override
    public <E> boolean checkName(Class<E> name) throws DependencyException {
        return factories.containsKey(name);
    }

    @Override
    public <E> E addChecking(Class<E> name, E value) {
        return (E)factories.putIfAbsent(name, value);
    }

    @Override
    public <E> E get(Class<E> name) {
        return (E)factories.get(name);
    }

    @Override
    public <E> E addChecking(Class<E> name, FactoryParamsE fp) {
        return (E)factories.putIfAbsent(name, fp);
    }

    @Override
    public Object[] checkToGetParametersE(Object[] parameters) {
        throw new UnsupportedOperationException("Not supported here.");
    }
    
    
    
   
}
