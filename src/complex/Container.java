/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package complex;

import common.ContainersFactories;
import common.ContainersConstants;
import common.Containers;
import common.DependencyException;
import common.FactoryParamsE;

/**
 *
 * @author santi
 */
public class Container implements Injector{
    
    private static Containers containerConstants, containerFactories;
    protected static Container container;
    
    protected Container(){
        
    }
    
    public static Container getInstance(){
        if(container == null){
            container = new Container();    
        }
        containerConstants = new ContainersConstants();
        containerFactories = new ContainersFactories();
        return container;
    }
    
    @Override
    public <E> void registerConstant(Class<E> name, E value) throws DependencyException {
        if (name.toString().isEmpty()){
            throw new DependencyException("The name is empty");
        }else if( containerConstants.addChecking(name, value) != null ){
            throw new DependencyException("The name is already exists");
        }
    }

    @Override
    public <E> void registerFactory(Class<E> name, Factory<? extends E> creator, Class<?>... parameters) throws DependencyException {
        for(Class<?> params : parameters){
            if(!containerConstants.checkName(params)){
                throw new DependencyException("The associated not exists");
            }
        }
        if(parameters.length == 0){
            throw new DependencyException("You need parameters");
        }
        FactoryParamsE fp = new FactoryParamsE(creator, parameters);
        if(containerFactories.addChecking(name, fp) != null){
            throw new DependencyException("The name is already exists");
        }
    }

    @Override
    public <E> E getObject(Class<E> name) throws DependencyException {
        if(containerConstants.checkName(name)){
            return containerConstants.get(name);
        }else if(containerFactories.checkName(name)){
            FactoryParamsE fp = (FactoryParamsE)containerFactories.get(name);
            Object[] parameters = containerConstants.checkToGetParametersE(fp.getParameters());
            if(parameters == null) throw new DependencyException("The arguments are not correct");
            return (E)fp.getCreator().create(parameters);
        }else{
            throw new DependencyException("The object wasn't found with name: "
            + name);
        }
    }
    
}
