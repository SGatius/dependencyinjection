/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import common.ContainersFactories;
import common.ContainersConstants;
import common.Containers;
import common.DependencyException;
import common.FactoryParams;

/**
 *
 * @author santi
 */
public class Container implements Injector{

    private static Containers containerConstants, containerFactories;
    private static Container container;
    
    /**
     *An empty constructor
     */
    protected Container(){}
    
    /**
     * Obtain an unique instance about Container
     * @return a Container
     */
    public static Container getInstance(){
        if (container == null) {
            container = new Container();
            containerConstants = new ContainersConstants();
            containerFactories = new ContainersFactories();
        }
        return container;
    }
    
    @Override
    public void registerConstant(String name, Object value) throws DependencyException {
        if(name.isEmpty()){
            throw new DependencyException("The name is empty");
        }
        else if(containerConstants.addChecking(name, value) != null){
            throw new DependencyException("The name is already exists"); 
        }
        
    }

    @Override
    public void registerFactory(String name, Factory creator, String... parameters) throws DependencyException {
        for(String params : parameters){
            if(params.isEmpty()){
                throw new DependencyException("Some parameters are empty");
            }else if(!containerConstants.checkName(params)){
            throw new DependencyException("This associated not exist");
            }
        }
        FactoryParams fp = new FactoryParams(creator, parameters);
        if (containerFactories.addChecking(name, fp) != null) {
            throw new DependencyException("The name is already exists");            
        }
    }

    @Override
    public Object getObject(String name) throws DependencyException {
        if (containerConstants.checkName(name)) {
            return containerConstants.get(name);
        } else if (containerFactories.checkName(name)){
            FactoryParams fp = (FactoryParams) containerFactories.get(name);
            Object[] parameters = containerConstants
                    .checkToGetParameters(fp.getParameters());
            if (parameters == null) throw new DependencyException("The arguments are not correct");
            return fp.getCreator().create(parameters);
        }else{
            throw new DependencyException("The object wasn't found with name: "
            + name);
        }
    }    
}
