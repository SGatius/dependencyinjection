/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import common.DependencyException;

/**
 *
 * @author santi
 */
public interface Injector { 
    
    /**
     * Register a new constant
     * @param name associated with the value
     * @param value value that register
     * @throws DependencyException
     */
    void registerConstant(String name, Object value)
            throws DependencyException;
    
    /**
     * Register a new Factory
     * @param name associated with the factory
     * @param creator instance of factory
     * @param parameters set of names associated with the values
     * @throws DependencyException
     */
    void registerFactory(String	name,
            Factory creator, 
            String...	parameters)
            throws DependencyException;
    
    /**
     * Obtain the corresponding object
     * @param name associated with an object
     * @return the corresponding object associated with the name
     * @throws DependencyException
     */
    Object getObject(String name)
        throws DependencyException;
    
}