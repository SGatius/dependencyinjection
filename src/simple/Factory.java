/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simple;

import common.DependencyException;

/**
 *
 * @author santi
 */
public interface Factory {
    
    /**
     * Create a new instance
     * @param parameters
     * @return a new Factory's object
     * @throws DependencyException
     */
    Object create(Object... parameters) throws DependencyException;
    
}
